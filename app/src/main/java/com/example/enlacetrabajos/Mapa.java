package com.example.enlacetrabajos;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatActivity;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

public class Mapa extends AppCompatActivity {

    IMapController mapaControlador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        MapView mapa = (MapView) findViewById(R.id.mapa);

        Context ctx = getApplicationContext();

        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        mapa.setTileSource(TileSourceFactory.MAPNIK);
        mapa.setMultiTouchControls(true);

        GeoPoint miPunto = new GeoPoint(4.659, -74.091);

        Marker miMarcador = new Marker(mapa);
        miMarcador.setPosition(miPunto);
        mapa.getOverlays().add(miMarcador);

        GeoPoint miPunto2 = new GeoPoint(4.654, -74.090);
        Marker miMarcador2 = new Marker(mapa);
        miMarcador2.setPosition(miPunto2);
        mapa.getOverlays().add(miMarcador2);

        mapaControlador = mapa.getController();
        mapaControlador.setZoom(16.0);
        mapaControlador.setCenter(miPunto);

    }
}
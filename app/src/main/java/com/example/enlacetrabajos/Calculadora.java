package com.example.enlacetrabajos;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;

import org.mariuszgromada.math.mxparser.Expression;

public class Calculadora extends AppCompatActivity implements View.OnClickListener {


    TextView expresion, resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);


        MaterialButton btn_0, btn_1, btn_2, btn_3, btn_4, btn_5, btn_6, btn_7, btn_8, btn_9;
        MaterialButton btn_C, btn_AbreParentesis, btn_CierraParente, btn_Ac, btn_punto;
        MaterialButton btn_multiplica, btn_divide, btn_suma, btn_resta, btn_igual;


        //2 Relacionar el objeto con el elemento en la vista
        expresion = findViewById(R.id.expresion);
        resultado = findViewById(R.id.resultado);
        btn_0 = findViewById(R.id.bt_0);
        btn_1 = findViewById(R.id.bt_1);
        btn_2 = findViewById(R.id.bt_2);
        btn_3 = findViewById(R.id.bt_3);
        btn_4 = findViewById(R.id.bt_4);
        btn_5 = findViewById(R.id.bt_5);
        btn_6 = findViewById(R.id.bt_6);
        btn_7 = findViewById(R.id.bt_7);
        btn_8 = findViewById(R.id.bt_8);
        btn_9 = findViewById(R.id.bt_9);
        btn_C = findViewById(R.id.bt_C);
        btn_AbreParentesis = findViewById(R.id.bt_AbreParentesis);
        btn_CierraParente = findViewById(R.id.bt_CierraParente);
        btn_Ac = findViewById(R.id.bt_Ac);
        btn_punto = findViewById(R.id.bt_punto);
        btn_multiplica = findViewById(R.id.bt_multiplica);
        btn_divide = findViewById(R.id.bt_divide);
        btn_suma = findViewById(R.id.bt_suma);
        btn_resta = findViewById(R.id.bt_resta);
        btn_igual = findViewById(R.id.bt_igual);

        //3. convertir el boton con un listers
        btn_0.setOnClickListener(this);
        btn_1.setOnClickListener(this);
        btn_2.setOnClickListener(this);
        btn_3.setOnClickListener(this);
        btn_4.setOnClickListener(this);
        btn_5.setOnClickListener(this);
        btn_6.setOnClickListener(this);
        btn_7.setOnClickListener(this);
        btn_8.setOnClickListener(this);
        btn_9.setOnClickListener(this);
        btn_C.setOnClickListener(this);
        btn_AbreParentesis.setOnClickListener(this);
        btn_CierraParente.setOnClickListener(this);
        btn_Ac.setOnClickListener(this);
        btn_punto.setOnClickListener(this);
        btn_multiplica.setOnClickListener(this);
        btn_divide.setOnClickListener(this);
        btn_suma.setOnClickListener(this);
        btn_resta.setOnClickListener(this);
        btn_igual.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        //1.1 crear objeto del click
        MaterialButton boton = (MaterialButton) view;
        String texto = boton.getText().toString();

        switch (texto) {
            case "C":
                expresion.setText("0");
                break;
            case "Ac":
                expresion.setText("0");
                resultado.setText("0");
                break;
            case "=":
                String valor = expresion.getText().toString();
                Expression analizador = new Expression(valor);
                double resultado2 = analizador.calculate();

                if (Double.isNaN(resultado2)) {
                    resultado.setText("Por Favor Ingrese un valor correcto");

                } else {
                    resultado.setText(Double.toString(resultado2));

                }
            default:
                String valorActualExpresion = expresion.getText().toString();
                if (valorActualExpresion.equals("0")) {
                    expresion.setText(texto);
                } else {
                    String NuevoValor = valorActualExpresion + texto;
                    expresion.setText(NuevoValor);
                }
                break;
        }

    }
}
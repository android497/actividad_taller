package com.example.enlacetrabajos;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class IndexPrincipal extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index_principal);

        //1. Cargar el elemento
        Button btnCalculadora = findViewById(R.id.btnCalculadora);
        Button btnMapa = findViewById(R.id.btnMapa);
        Button btnEquipos = findViewById(R.id.btnEquipos);


        //Para el Direccionamiento a la otra vista
        btnCalculadora.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), Calculadora.class);
            startActivity(intent);
        });

        btnMapa.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), Mapa.class);
            startActivity(intent);
        });

        btnEquipos.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), EquiposDeFutbol.class);
            startActivity(intent);
        });

    }
}